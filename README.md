# Overview

A demo application that lets you manage patents in a decentralized way
This project contain the PattentFiling solidity contract.

* Microservices submodule

# Continuous integration

The gitlab allows to launch tests on this project with ethereumjs-testrpc,  create and deploy automatically the documentation of this contract

[see the pipeline](https://gitlab.com/matkt/patent-filing-contract/pipelines)

When the pipeline is terminated you can view and share the new documentation 

[see the documentation](https://matkt.gitlab.io/patent-filing-contract/)

# Technologies used :
 * gitlab-ci
 * docker
 * Ganache-cli
 * Truffle
 * Openzeppelin-solidity
 * Solidity-docgen
 * Docusaurus

