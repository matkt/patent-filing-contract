/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
const siteConfig = {
  title: 'Patent filing documentation' /* title for your website */,
  tagline: 'This project aims to facilitate the management of patents. The use of the blockchain will allow complete traceability and guarantee immunity. A person can add a patent and wait for a donor to accept this addition. The supplier must verify that it is not plagiarism or an invalid patent. This project is a proof of concept.',
  url: 'https://gitlab.com/matkt' /* your website url */,
  baseUrl: '/patent-filing-contract/' /* base url for your project */,
  
  // Used for publishing and more
  projectName: 'patent-filing-contract',
  organizationName: 'karim t.',
  // For no header links in the top nav bar -> headerLinks: [],
  headerLinks: [
    {doc: 'PatentFiling', label: 'PatentFiling'}
  ],

  /* path to images for header/footer */
  headerIcon: 'img/docusaurus.svg',
  favicon: 'img/favicon.png',

  /* colors for website */
  colors: {
    primaryColor: '#1D2041',
    secondaryColor: '#3f468d',
  },

  /* custom fonts for website */
  /*fonts: {
    myFont: [
      "Times New Roman",
      "Serif"
    ],
    myOtherFont: [
      "-apple-system",
      "system-ui"
    ]
  },*/

  // This copyright info is used in /core/Footer.js and blog rss/atom feeds.
  copyright:
    'Copyright © ' +
    new Date().getFullYear() +
    ' Karim T.',

  highlight: {
    // Highlight.js theme to use for syntax highlighting in code blocks
    theme: 'default',
  },

  // Add custom scripts here that would be placed in <script> tags
  scripts: ['https://buttons.github.io/buttons.js'],

  /* On page navigation for the current documentation page */
  onPageNav: 'separate',



};

module.exports = siteConfig;
