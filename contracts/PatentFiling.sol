pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/ownership/rbac/RBAC.sol";

/**
 * @title PatentFiling
 * @author Karim TAAM
 * @dev This contract aims to facilitate the management of patents.
 * The use of the blockchain will allow complete traceability and guarantee immunity.
 * A person can add a patent and wait for a donor to accept this addition.
 * The supplier must verify that it is not plagiarism or an invalid patent. This project is a proof of concept.' 
 * The patent is submit with ipfs hash.
 * See the tests PatentFiling.test.js for specific usage examples.
 */
contract PatentFiling is RBAC {

   /**
   * @dev A constant role name for indicating Approver.
   */
  string public constant ROLE_APPROVER = "Approver";

  /**
   * @dev A constant role name for indicating Depositor.
   */
  string public constant ROLE_DEPOSITOR = "Depositor";

  struct Patent{
    address depositorAddress;
    address approverAddress;
    bool isCreated;
  }

  mapping (bytes32 => Patent) private patents;

  event NewPatentRecorded(address indexed depositorAddress, bytes32 id);

  event NewPatentApproved(address indexed approverAddress, bytes32 id);

  /**
   * @dev modifier to scope access to approver
   * // reverts
   */
  modifier onlyApprover()
  {
    checkRole(msg.sender, ROLE_APPROVER);
    _;
  }

  /**
  * @dev modifier to check if string is empty
  * @param _value string to check
  * // reverts
  */
  modifier nonEmptyString(string _value){
    require(bytes(_value).length>0);
    _;
  }

  /**
  * @dev modifier to check if address is empty
  * @param _value address to check
  * // reverts
  */
  modifier nonEmptyAddress(address _value){
    require(_value != address(0));
    _;
  }


  constructor()
   public {
    addRole(msg.sender, ROLE_APPROVER);
  }


  /**
  * @dev Add a new Approver authorized to approved patent
  * @param _approver address of the new approver
  */
  function addApprover(address _approver)
   public
   nonEmptyAddress(_approver)
   onlyApprover
  {
    addRole(_approver, ROLE_APPROVER);
  }

  /**
  * @dev Record a new patent
  * @param _ipfsHash ipfs hash of the patent
  */
  function recordPatent(string _ipfsHash)
    public
    nonEmptyString(_ipfsHash)
  {
    bytes32 hash = keccak256(bytes(_ipfsHash));
    assert(!patents[hash].isCreated);
    patents[hash] = Patent(msg.sender, address(0), true);
    emit NewPatentRecorded(msg.sender, hash);
  }

  /**
  * @dev Use this function to check if patent is approved
  * @param _ipfsHash the ipfs hash of patent
  * @return is pattend approved
  */
  function isPatentApproved(string _ipfsHash)
    public
    view
    nonEmptyString(_ipfsHash)
    returns(bool)
  {
    Patent memory patent = patents[keccak256(bytes(_ipfsHash))];
    return patent.isCreated && patent.approverAddress != address(0);
  }

  /**
  * @dev Use this function to approve patent.
  * Addresses of the approver and depositor must be different
  * @param _ipfsHash the ipfs hash of patent
  */
  function approvePatent(string _ipfsHash)
    public
    nonEmptyString(_ipfsHash)
    onlyApprover
  {
    bytes32 hash = keccak256(bytes(_ipfsHash));
    Patent storage patent = patents[hash];
    assert(patent.isCreated);
    assert(patent.depositorAddress != msg.sender);
    patent.approverAddress = msg.sender;
    emit NewPatentApproved(msg.sender, hash);
  }

  /**
  * @dev Use this function to get the approver of a specific patent
  * @param _ipfsHash the ipfs hash of patent
  * @return the address of the approver (if empty not approver found)
  */
  function getApproverOfPatent(string _ipfsHash)
    public
    view
    nonEmptyString(_ipfsHash)
    returns(address)
  {
    Patent memory patent = patents[keccak256(bytes(_ipfsHash))];
    assert(patent.isCreated);
    assert(patent.approverAddress!=address(0));
    return patent.approverAddress;
  }

  /**
  * @dev Use this function to get the depositor of a specific patent
  * @param _ipfsHash the ipfs hash of patent
  * @return the address of the depositor
  */
  function getDepositorOfPatent(string _ipfsHash)
    public
    view
    nonEmptyString(_ipfsHash)
    returns(address)
  {
    Patent memory patent = patents[keccak256(bytes(_ipfsHash))];
    assert(patent.isCreated);
    return patent.depositorAddress;
  }


}
