import expectThrow from "../node_modules/openzeppelin-solidity/test/helpers/expectThrow";
import expectEvent from "../node_modules/openzeppelin-solidity/test/helpers/expectEvent";
import assertRevert from '../node_modules/openzeppelin-solidity/test/helpers/assertRevert';
import assertJump from '../node_modules/openzeppelin-solidity/test/helpers/assertJump';

const PatentFilingMock = artifacts.require('PatentFiling');

require('chai')
  .use(require('chai-as-promised'))
.should();

contract('PatentFiling', function (accounts) {

  let mock;

  const [
    owner,
    anyone,
    anyone2,
  ] = accounts;

  const ipfsHash1 = "afccfeb7726160d74f8a18407853846aab2ebd57db1dc32409acd6aefc7c4b33";
  const ipfsHash2 = "bfccfeb7726160d74f8a18407853846aab2ebd57db1dc32409acd6aefc7c4b33";
  const ipfsHash3 = "cfccfeb7726160d74f8a18407853846aab2ebd57db1dc32409acd6aefc7c4b33";
  const ipfsHash4 = "dfccfeb7726160d74f8a18407853846aab2ebd57db1dc32409acd6aefc7c4b33";

  before(async function () {
    mock = await PatentFilingMock.new();
  });

  context('in normal conditions', () => {
      it('should be possible to record a new patent', async function () {
          await expectEvent.inTransaction(
                mock.recordPatent(ipfsHash1, { from: anyone }),
                'NewPatentRecorded'
          );
       });
       it('should be possible to approve a new patent for approver address', async function () {
           await expectEvent.inTransaction(
                mock.approvePatent(ipfsHash1, { from: owner }),
                'NewPatentApproved'
           );
        });
       it('should be possible for anyone to check if patent is approved', async function () {
            await expectEvent.inTransaction(
                mock.recordPatent(ipfsHash2, { from: anyone }),
                'NewPatentRecorded'
            );
            const isPatentApprovedBefore = await mock.isPatentApproved(ipfsHash2);
            await expectEvent.inTransaction(
                mock.approvePatent(ipfsHash2, { from: owner }),
                'NewPatentApproved'
            );
            const isPatentApprovedAfter = await mock.isPatentApproved(ipfsHash2);
            isPatentApprovedBefore.should.be.equal(false);
            isPatentApprovedAfter.should.be.equal(true);
      });
      it('should be possible for approver to add a new approver', async function () {
            await expectEvent.inTransaction(
                 mock.addApprover(anyone, { from: owner }),
                 'RoleAdded'
            );
      });
      it('should be possible to retrieve address\'s approver of a patent', async function () {
            const approverAddress = await mock.getApproverOfPatent(ipfsHash1);
            approverAddress.should.be.equal(owner);
      });
      it('should be possible to retrieve address\'s depositor of a patent', async function () {
            const depositorAddress = await mock.getDepositorOfPatent(ipfsHash1);
            depositorAddress.should.be.equal(anyone);
      });
   });
   context('in not normal conditions', () => {
       it('should not be possible to record and approved a patent with the same address', async function () {
           await expectEvent.inTransaction(
                 mock.recordPatent(ipfsHash3, { from: owner }),
                 'NewPatentRecorded'
           );
           await expectThrow(
                 mock.approvePatent(ipfsHash3, { from: owner }),
           );
       });
       it('should not be possible to record twice the same patent', async function () {
          await expectEvent.inTransaction(
                  mock.recordPatent(ipfsHash4, { from: owner }),
                  'NewPatentRecorded'
          );
          await expectThrow(
                  mock.recordPatent(ipfsHash4, { from: owner }),
          );
        });
    });
});
