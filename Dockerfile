# Node image
FROM node:latest

# Install Truffle
RUN npm install -g truffle && npm config set bin-links false

# Create code directory
RUN mkdir /source

# Set working directory
WORKDIR /source

ADD . /source
RUN cd /source

RUN npm install 
RUN truffle compile

ENTRYPOINT [ "truffle","migrate", "--network", "docker", "--reset" ]
